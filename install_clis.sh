# /bin/bash

echo "Checking if Google Cloud SDK is installed."
if ! [ -x "$(command -v gcloud)" ]; then
  echo "Gcloud is not installed. Installing gcloud."
  curl https://sdk.cloud.google.com | bash
  exec -l $SHELL
fi

echo "Checking if Kubectl Cli is installed."
if ! [ -x "$(command -v kubectl)" ]; then
  echo "Kubectl is not installed. Installing Kubectl."
  gcloud components install kubectl --quiet
fi

echo "Checking if Terraform is installed."
if ! [ -x "$(command -v terraform)" ]; then
  echo "Terraform is not installed. Installing Terraform."

  # Prerequisites
  if [ "$(uname)" == "Darwin" ]; then
      brew install jq curl unzip
  # For Linux
  elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
      sudo apt-get install --assume-yes jq curl unzip
  fi

  # Get URLs for most recent versions
  # For OS-X
  if [ "$(uname)" == "Darwin" ]; then
      terraform_url=$(curl https://releases.hashicorp.com/index.json | jq '{terraform}' | egrep "darwin.*64" | sort --version-sort -r | head -1 | awk -F[\"] '{print $4}')
  # For Linux
  elif [ "$(expr substr $(uname -s) 1 5)" == "Linux" ]; then
      terraform_url=$(curl https://releases.hashicorp.com/index.json | jq '{terraform}' | egrep "linux.*amd64" | sort --version-sort -r | head -1 | awk -F[\"] '{print $4}')
  fi

  # Download Terraform. URI: https://www.terraform.io/downloads.html
  echo "Downloading $terraform_url."
  curl -o terraform.zip $terraform_url

  unzip terraform.zip -d executables/

  mv executables/terraform /usr/local/bin/terraform

  chmod +x /usr/local/bin/terraform

  rm terraform.zip

  rm -rf executables/

fi

echo "Checking if Helm is installed."
if ! [ -x "$(command -v helm)" ]; then
  echo "Helm is not installed. Installing Helm."
  curl -LO https://git.io/get_helm.sh
  chmod 700 get_helm.sh
  ./get_helm.sh
fi
