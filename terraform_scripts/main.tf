/*
This terraform script calls another module, terraform_gcp_gke, to provision a GKE cluster.
The module does the following:
  Enables Google Cloud APIs required for creating GKE cluster using APIs.
  Creates a Google Cloud service account that will be used by the custer as the main service account.
  Assigns appropriate permissions to the service account.
  Provisions a GKE cluster using configurations provided via terraform variables.
  Get GKE cluster credentials and set kubectl to use it.
After the cluster is provisioned, it does the following:
  Installs Helm in environment.
  Setup service account for tiller.
  Instantiate helm in cluster with tiller enabled.
  Update local helm repositories.
  Install nginx ingress in namespace nginx-ingress.
  Install cert manager in namespace cert-manager.
  Create a production cluster issuer for cert manager with email set up as info@samwelopiyo.guru.
*/


provider "google" {
  region  = "${var.region}"
  zone    = "${var.zone}"
  project = "${var.project_id}"
}

provider "kubernetes" {
  host     = "${module.terraform_gcp_gke.google_container_cluster_cluster_endpoint}"
  username = "${var.gke_master_user}"
  password = "${var.gke_master_password}"

  client_certificate     = "${base64decode(module.terraform_gcp_gke.google_container_cluster_client_certificate)}"
  client_key             = "${base64decode(module.terraform_gcp_gke.google_container_cluster_client_key)}"
  cluster_ca_certificate = "${base64decode(module.terraform_gcp_gke.google_container_cluster_cluster_ca_certificate)}"
}

provider "helm" {
    kubernetes {
      host     = "${module.terraform_gcp_gke.google_container_cluster_cluster_endpoint}"
      username = "${var.gke_master_user}"
      password = "${var.gke_master_password}"

      client_certificate     = "${base64decode(module.terraform_gcp_gke.google_container_cluster_client_certificate)}"
      client_key             = "${base64decode(module.terraform_gcp_gke.google_container_cluster_client_key)}"
      cluster_ca_certificate = "${base64decode(module.terraform_gcp_gke.google_container_cluster_cluster_ca_certificate)}"
    }
}

module "terraform_gcp_gke" {
  source       = "git::https://github.com/SamwelOpiyo/terraform_gcp_gke//?ref=v0.1.0-beta.5"
  region       = "${var.region}"
  zone         = "${var.zone}"
  project_name = "${var.project_name}"
  project_id   = "${var.project_id}"

  project_services_to_enable    = "${var.project_services_to_enable}"
  cluster_name                  = "${var.cluster_name}"
  cluster_description           = "${var.cluster_description}"
  cluster_location              = "${var.cluster_location}"
  cluster_oauth_scopes          = "${var.cluster_oauth_scopes}"
  node_locations                = "${var.node_locations}"
  min_master_version            = "${var.min_master_version}"
  node_version                  = "${var.node_version}"
  cluster_initial_node_count    = "${var.cluster_initial_node_count}"
  node_disk_size_gb             = "${var.node_disk_size_gb}"
  node_disk_type                = "${var.node_disk_type}"
  gke_master_user               = "${var.gke_master_user}"
  gke_master_password           = "${var.gke_master_password}"
  gke_node_machine_type         = "${var.gke_node_machine_type}"
  has_preemptible_nodes         = "${var.has_preemptible_nodes}"
  gke_label_env                 = "${var.gke_label_env}"
  client_email                  = "${var.client_email}"
  service_account_iam_roles     = "${var.cluster_service_account_iam_roles}"
  kubernetes_logging_service    = "${var.kubernetes_logging_service}"
  kubernetes_monitoring_service = "${var.kubernetes_monitoring_service}"

  is_http_load_balancing_disabled        = "${var.is_http_load_balancing_disabled}"
  is_kubernetes_dashboard_disabled       = "${var.is_kubernetes_dashboard_disabled}"
  is_horizontal_pod_autoscaling_disabled = "${var.is_horizontal_pod_autoscaling_disabled}"
  is_istio_disabled                      = "${var.is_istio_disabled}"
  is_cloudrun_disabled                   = "${var.is_cloudrun_disabled}"
  daily_maintenance_start_time           = "${var.daily_maintenance_start_time}"
  is_vertical_pod_autoscaling_enabled    = "${var.is_vertical_pod_autoscaling_enabled}"
  is_cluster_autoscaling_enabled         = "${var.is_cluster_autoscaling_enabled}"
  cluster_autoscaling_cpu_max_limit      = "${var.cluster_autoscaling_cpu_max_limit}"
  cluster_autoscaling_cpu_min_limit      = "${var.cluster_autoscaling_cpu_min_limit}"
  cluster_autoscaling_memory_max_limit   = "${var.cluster_autoscaling_memory_max_limit}"
  cluster_autoscaling_memory_min_limit   = "${var.cluster_autoscaling_memory_min_limit}"
}

data "helm_repository" "stable" {
    name = "stable"
    url  = "https://kubernetes-charts.storage.googleapis.com"
}

data "kubernetes_service" "nginx-ingress-controller" {
  metadata {
    name      = "nginx-ingress-controller"
    namespace = "nginx-ingress"
  }

  depends_on = [
    "helm_release.nginx-ingress",
  ]
}

resource "helm_release" "nginx-ingress" {
  name       = "nginx-ingress"
  namespace  = "nginx-ingress"
  repository = "${data.helm_repository.stable.metadata.0.name}"
  chart      = "nginx-ingress"

  set {
    name  = "rbac.create"
    value = true
  }

  depends_on = [
    "null_resource.install-dependencies",
  ]
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  namespace  = "cert-manager"
  repository = "${data.helm_repository.stable.metadata.0.name}"
  chart      = "cert-manager"

  depends_on = [
    "null_resource.install-dependencies",
  ]
}

resource "helm_release" "prometheus-monitoring-stack" {
  name       = "prometheus-monitoring-stack"
  namespace  = "monitoring"
  repository = "${data.helm_repository.stable.metadata.0.name}"
  chart      = "prometheus-operator"

  set {
    name  = "grafana.ingress.enabled"
    value = true
  }

  set {
    name  = "grafana.ingress.hosts[0]"
    value = "grafana.${data.kubernetes_service.nginx-ingress-controller.load_balancer_ingress[0].ip}.nip.io"
  }

  set {
    name  = "grafana.ingress.path"
    value = "/"
  }

  set {
    name  = "grafana.ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "nginx"
  }

  # --set=grafana.ingress.tls[0].hosts[0]=grafana.${data.kubernetes_service.nginx-ingress-controller.load_balancer_ingress[0].ip}.nip.io
  # --set=kibana.ingress.tls[0].secretName=example-elk-tls

  depends_on = [
    "data.kubernetes_service.nginx-ingress-controller",
  ]
}

resource "helm_release" "elastic-stack" {
  name       = "elastic-stack"
  namespace  = "logging"
  repository = "${data.helm_repository.stable.metadata.0.name}"
  chart      = "elastic-stack"
  timeout    = 1500

  set {
    name  = "logstash.enabled"
    value = false
  }

  set {
    name  = "fluent-bit.enabled"
    value = true
  }

  set {
    name  = "kibana.env.ELASTICSEARCH_HOSTS"
    value = "http://elastic-stack-elasticsearch-client.logging.svc.cluster.local:9200"
  }

  set {
    name  = "fluent-bit.backend.type"
    value = "es"
  }

  set {
    name  = "fluent-bit.backend.es.host"
    value = "elastic-stack-elasticsearch-client.logging.svc.cluster.local"
  }

  set {
    name  = "kibana.ingress.enabled"
    value = true
  }

  set {
    name  = "kibana.ingress.hosts[0]"
    value = "elk.${data.kubernetes_service.nginx-ingress-controller.load_balancer_ingress[0].ip}.nip.io"
  }

  set {
    name  = "kibana.ingress.annotations.kubernetes\\.io/ingress\\.class"
    value = "nginx"
  }

  # --set=kibana.ingress.annotations."nginx\.ingress\.kubernetes\.io/auth-url"="https://httpbin.org/basic-auth/elkuser/qazwsx12345"
  # --set=kibana.ingress.tls[0].hosts[0]=elk.${data.kubernetes_service.nginx-ingress-controller.load_balancer_ingress[0].ip}.nip.io
  # --set=kibana.ingress.tls[0].secretName=example-elk-tls

  depends_on = [
    "data.kubernetes_service.nginx-ingress-controller",
  ]
}

resource "null_resource" "install-dependencies" {
  triggers = {
    host = "${md5(module.terraform_gcp_gke.google_container_cluster_cluster_endpoint)}"
  }

  provisioner "local-exec" {
    command = <<EOF

      echo "Checking if Helm is installed."
      if ! [ -x "$(command -v helm)" ]; then
        echo "Helm is not installed. Installing Helm."
        curl -LO https://git.io/get_helm.sh
        chmod 700 get_helm.sh
        ./get_helm.sh
      fi

      echo "Setting Up a Service Account for Tiller"
      kubectl create serviceaccount --namespace kube-system tiller
      kubectl create clusterrolebinding tiller-cluster-rule --clusterrole=cluster-admin --serviceaccount=kube-system:tiller

      echo "Upgrading and Installing tiller..."
      helm init --service-account tiller --upgrade --wait

      echo "Updating helm repositories"
      helm repo update

      echo "Installing Cert Manager crds."
      kubectl apply -f https://raw.githubusercontent.com/jetstack/cert-manager/release-0.6/deploy/manifests/00-crds.yaml

      EOF
  }

  depends_on = [
    "module.terraform_gcp_gke",
  ]
}

resource "null_resource" "install-cluster-issuer" {
  triggers = {
    host = "${md5(module.terraform_gcp_gke.google_container_cluster_cluster_endpoint)}"
  }

  provisioner "local-exec" {
    command = <<EOF

      echo "Creating production cluster issuer."
      kubectl apply -f - <<HERE
      apiVersion: certmanager.k8s.io/v1alpha1
      kind: ClusterIssuer
      metadata:
        name: letsencrypt-prod
      spec:
        acme:
          # The ACME server URL
          server: https://acme-v02.api.letsencrypt.org/directory
          # Email address used for ACME registration
          email: info@samwelopiyo.guru
          # Name of a secret used to store the ACME account private key
          privateKeySecretRef:
            name: letsencrypt-prod
          # Enable the HTTP-01 challenge provider
          http01: {}
HERE
      EOF
  }

  depends_on = [
    "helm_release.cert-manager",
  ]
}
