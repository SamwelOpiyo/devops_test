# Infrastructure Setup(First Time Setup).

## Dependencies.

* Google Cloud SDK: https://cloud.google.com/sdk/docs/
* Kubectl Cli: https://kubernetes.io/docs/tasks/tools/install-kubectl/
* Terraform:
  * https://learn.hashicorp.com/terraform/getting-started/install.html
  * https://www.terraform.io/downloads.html
* Helm: https://helm.sh/docs/using_helm/#installing-the-helm-client

To install the three, run:

```
chmod +x install_clis.sh
./install_clis.sh
```

## Environment Variables.

Set the following environment variables. (Replace {project_id} with GCP Project Id.)

```
export TF_ADMIN="{{project_id}}"
```

## Infrastructure Creation and Deployment.

### Google Cloud SDK Setup.

Run the following command which will open google login page on a browser. Login using your account credentials to authenticate Google Cloud SDK.

```
gcloud auth application-default login
```

### Configuring the remote bucket.

Create the remote backend bucket in Cloud Storage for storage of the terraform.tfstate file:

```
gsutil mb -p ${TF_ADMIN} gs://${TF_ADMIN}-terraform
```

To activate the version for this bucket.

```
gsutil versioning set on gs://${TF_ADMIN}-terraform
```

Navigate to terraform_scripts directory.

```
cd terraform_scripts/
```

Update the backend.tf file appropriately.

Once these steps are followed, we initialize the backend.

```
terraform init
```

### Infrastructure Creation.

Edit variables.tfvars appropriately before continuing.

Now we are ready to execute the plan:

```
terraform plan -var-file variables.tfvars
```

and apply:

```
terraform apply -var-file variables.tfvars
```

To get nginx ingress loadbalancer IP that is serving the applications:

```
terraform output nginx-ingress-endpoint
```

If any A records need to be added, they should point to this IP.

# Infrastructure Setup(Subsequent Setups).

## Environment Variables.

Set the following environment variables. (Replace {project_id} with GCP Project Id.)

```
export TF_ADMIN="{{project_id}}"
```

## Infrastructure Creation and Deployment.

### Google Cloud SDK Setup.

Run the following command which will open google login page on a browser. Login using your account credentials to authenticate Google Cloud SDK.

```
gcloud auth application-default login
```

### Configuring the remote bucket.

Create the remote backend bucket in Cloud Storage if it does not exist. This bucket is used for storage of the terraform.tfstate file:

```
gsutil mb -p ${TF_ADMIN} gs://${TF_ADMIN}-terraform
```

To activate the version for this bucket.

```
gsutil versioning set on gs://${TF_ADMIN}-terraform
```

Navigate to terraform_scripts directory.

```
cd terraform_scripts/
```

Update the backend.tf file appropriately.

Once these steps are followed, we initialize the backend.

```
terraform init
```

### Infrastructure Creation.

Edit variables.tfvars appropriately before continuing.

Now we are ready to execute the plan:

```
terraform plan -var-file variables.tfvars
```

and apply:

```
terraform apply -var-file variables.tfvars
```

To get nginx ingress loadbalancer IP that is serving the applications:

```
terraform output nginx-ingress-endpoint
```

If any A records need to be added, they should point to this IP.

# Cleaning Up.

To destroy everything, you need to perform the following actions.

- Destroy the infrastructure that we have created:

```
terraform destroy -var-file variables.tfvars
```
